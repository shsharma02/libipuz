# -*- indent-tabs-mode: nil -*-

# Continuous Integration configuration for libipuz
#
# This uses Freedesktop CI templates to generate container images for
# the pipeline's jobs: https://gitlab.freedesktop.org/freedesktop/ci-templates/
#
# Freedesktop CI Templates documentation: https://freedesktop.pages.freedesktop.org/ci-templates
#


# The SHAs here correspond to commits in the freedesktop/ci-templates repository.
# It doesn't change often, but you can update to newer SHAs if there are important
# changes there.
include:
  - remote: "https://gitlab.freedesktop.org/freedesktop/ci-templates/-/raw/c5626190/templates/fedora.yml"
  - remote: "https://gitlab.freedesktop.org/freedesktop/ci-templates/-/raw/80f87b3058efb75a1faae11826211375fba77e7f/templates/opensuse.yml"

variables:
  # When branching change the suffix to avoid conflicts with images
  # from the main branch
  BASE_TAG: "2024-03-13.0-master"
  FDO_UPSTREAM_REPO: "jrb/libipuz"
  RUST_STABLE: "1.75.0"
  RUSTUP_VERSION: "1.26.0"

# Stages in the CI pipeline in which jobs will be run
stages:
  - container-build
  - build
  - analysis
  - docs
  - deploy

default:
  interruptible: true

# Enable merge request pipelines and avoid duplicate pipelines
# https://docs.gitlab.com/ee/ci/yaml/index.html#switch-between-branch-pipelines-and-merge-request-pipelines
workflow:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS && $CI_PIPELINE_SOURCE == "push"
      when: never
    - if: '$CI_COMMIT_TAG'
    - if: '$CI_COMMIT_BRANCH'

.libipuz_fedora_x86_64:
  variables:
    FDO_DISTRIBUTION_TAG: "x86_64-${BASE_TAG}"
    FDO_DISTRIBUTION_VERSION: "37"

# Definition for the container image we'll use for the main build
.container.fedora@x86_64:
  extends:
    - .libipuz_fedora_x86_64
    - .fdo.container-build@fedora
  stage: "container-build"
  variables:
    FDO_DISTRIBUTION_PACKAGES: >-
      clang
      clang-analyzer
      clang-tools-extra
      compiler-rt
      gcc
      gettext
      git
      glib2-devel
      gobject-introspection-devel
      itstool
      json-glib-devel
      libasan
      llvm
      meson
      python3
      wget
    FDO_DISTRIBUTION_EXEC: >-
      bash ci/install-rust.sh --rustup-version ${RUSTUP_VERSION} \
                              --stable ${RUST_STABLE} \
                              --arch x86_64-unknown-linux-gnu &&
      bash ci/install-rust-tools.sh &&
      bash ci/install-grcov.sh &&
      rm -rf /root/.cargo /root/.cache    # cleanup compilation dirs; binaries are installed now

fedora-container@x86_64:
  extends:
    - .fdo.container-build@fedora
    - .container.fedora@x86_64
  stage: "container-build"

.libipuz_opensuse_x86_64:
  variables:
    FDO_DISTRIBUTION_TAG: "x86_64-${BASE_TAG}"
    FDO_DISTRIBUTION_VERSION: "tumbleweed"

# Builds a container image based on openSUSE Tumbleweed, with a bunch
# of tools for CI jobs and local development.
#
# If you want to use this image locally, run the
# ci/pull-container-image.sh script on your machine and follow its
# instructions.
opensuse-container@x86_64.stable:
  extends:
    - .libipuz_opensuse_x86_64
    - .fdo.container-build@opensuse@x86_64
  stage: "container-build"
  before_script:
    - source ./ci/env.sh
  variables:

    FDO_DISTRIBUTION_PACKAGES: >-
      clang
      clang-tools
      gcc
      gettext
      git
      glib2-devel
      gobject-introspection-devel
      itstool
      json-glib-devel
      libasan8
      llvm
      meson
      ninja
      python3-pip
      util-linux-systemd
      wget
      which

    FDO_DISTRIBUTION_EXEC: >-
      bash ci/install-python-tools.sh &&
      bash ci/install-rust.sh --rustup-version ${RUSTUP_VERSION} \
                              --stable ${RUST_STABLE} \
                              --arch x86_64-unknown-linux-gnu &&
      bash ci/install-rust-tools.sh &&
      bash ci/install-grcov.sh &&
      rm -rf /root/.cargo /root/.cache    # cleanup compilation dirs; binaries are installed now

fedora-x86_64:
  stage: build
  extends:
    - '.libipuz_fedora_x86_64'
    - '.fdo.distribution-image@fedora'
  needs:
    - job: fedora-container@x86_64
      artifacts: false
  variables:
    MESON_EXTRA_FLAGS: "--buildtype=debug" # -Dwerror=true
  script:
    - source ./ci/env.sh
    - meson setup ${MESON_EXTRA_FLAGS} --prefix /usr _build .
    - meson compile -C _build
    - meson test -C _build --print-errorlogs
  artifacts:
    reports:
      junit:
        - "_build/meson-logs/testlog.junit.xml"
        - "_build/tests/registryd/registryd-pytest.junit.xml"
    when: always
    name: "libipuz-${CI_COMMIT_REF_NAME}"
    paths:
      - "_build/meson-logs"

# Run static analysis on the code.
#
# The logs are part of the compilation stderr.
static-scan:
  stage: analysis
  extends:
    - '.libipuz_fedora_x86_64'
    - '.fdo.distribution-image@fedora'
  needs:
    - job: fedora-container@x86_64
      artifacts: false
  variables:
    MESON_EXTRA_FLAGS: "--buildtype=debug"
  script:
    - source ./ci/env.sh
    - meson setup ${MESON_EXTRA_FLAGS} --prefix /usr _scan_build .
    - ninja -C _scan_build scan-build
  artifacts:
    name: "libipuz-${CI_JOB_NAME}-${CI_COMMIT_REF_NAME}"
    when: always
    paths:
      - "_scan_build/meson-logs/scanbuild"

# Build and run with address sanitizer (asan).
asan-build:
  stage: analysis
  extends:
    - '.libipuz_fedora_x86_64'
    - '.fdo.distribution-image@fedora'
  needs:
    - job: fedora-container@x86_64
      artifacts: false
  tags: [ asan ]
  variables:
    MESON_EXTRA_FLAGS: "--buildtype=debug -Db_sanitize=address,undefined -Db_lundef=false"
    LSAN_OPTIONS: "verbosity=1:log_threads=1"
  script:
    - source ./ci/env.sh
    - CC=clang meson setup ${MESON_EXTRA_FLAGS} --prefix /usr _build .
    - meson compile -C _build
    - meson test -C _build --print-errorlogs
  artifacts:
    name: "libipuz-${CI_JOB_NAME}-${CI_COMMIT_REF_NAME}"
    when: always
    paths:
      - "${CI_PROJECT_DIR}/_build/meson-logs"

# Run the test suite and extract code coverage information.
#
# See the _coverage/ artifact for the HTML report.
coverage:
  stage: analysis
  extends:
    - '.libipuz_fedora_x86_64'
    - '.fdo.distribution-image@fedora'
  needs:
    - job: fedora-container@x86_64
      artifacts: false
  script:
    - source ./ci/env.sh
    - bash -x ./ci/build-with-coverage.sh
    - bash -x ./ci/gen-coverage.sh
  coverage: '/Coverage: \d+\.\d+/'
  artifacts:
    name: "libipuz-${CI_JOB_NAME}-${CI_COMMIT_REF_NAME}"
    expire_in: 2 days
    when: always
    reports:
      coverage_report:
        coverage_format: cobertura
        path: public/coverage.xml
    paths:
      - "_build/meson-logs"
      - public

# Render the development guide
devel-docs:
  extends:
    - '.libipuz_opensuse_x86_64'
    - '.fdo.distribution-image@opensuse'
  stage: docs
  needs:
    - job: opensuse-container@x86_64.stable
      artifacts: false
  script:
    - bash -x ./ci/gen-devel-docs.sh
  artifacts:
    paths:
      - public

# Publish the test coverage report and the rendered development guide with GitLab Pages
pages:
  stage: deploy
  needs: [ coverage, devel-docs ]
  script:
    # No-op, just to gitlab thinks there's something to do.
    # The jobs that this job depends on have already populated public/
    - echo
  artifacts:
    paths:
      - public
  rules:
    # Restrict to the main branch so not every branch tries to deploy the web site
    - if: ($CI_DEFAULT_BRANCH == $CI_COMMIT_BRANCH)
    # Alternatively, restrict it to the jrb namespace to avoid every fork pushing a set of pages by default
    # - if: ($CI_DEFAULT_BRANCH == $CI_COMMIT_BRANCH && $CI_PROJECT_NAMESPACE == "jrb")
