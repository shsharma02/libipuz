# libipuz.org

libipuz is a library for loading and saving the ipuz data format for
pencil-and-paper puzzles. It is written in C, and has dependencies on
glib and json-glib. It is licensed under terms of the
LGPLv2.1-or-later and the MIT licenses.

libipuz intends to have complete compatibility with the [ipuz
spec](http://ipuz.org) ([local
mirror](https://libipuz.org/ipuz-spec.html)), and areas where we don't
support something correctly are considered a bug. The areas where we
have extensions or clarifications are documented here:
* [Extensions to the ipuz spec that we support](ipuz-extensions.md)

This library was primarily written for use by [GNOME
Crosswords](https://gitlab.gnome.org/jrb/crosswords). As a result, it
only loads crossword-style ipuz puzzles. Other types are planned for
the future.

## Source code

The current version of libipuz is 0.4.5

* [Main libipuz repository](https://gitlab.gnome.org/jrb/libipuz)
* [Issue tracker](https://gitlab.gnome.org/jrb/libipuz/-/issues)
* [Release NEWS](https://gitlab.gnome.org/jrb/libipuz/-/blob/master/NEWS.md)
* [Release tarballs](https://gitlab.gnome.org/jrb/libipuz/-/releases)

## Design documents

* [Coordinate clarification](coords.md)
* [Filippine puzzles](filippine.md)
* [Parsing strategy](parsing-strategy.md)
* [Full Puzzle Kind](full-tree.md)

## Rustification

* [Rust in libipuz](rust.md)
* [Porting IPuzCharset to Rust](charset-rust.md)

## Coding style

* [Some style conventions](naming-conventions.md)

See http://ipuz.org/ for more information about the file format.
*ipuz is a trademark of Puzzazz, Inc., used with permission*
