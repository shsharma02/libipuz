/* ipuz-clue-sets.h
 *
 * Copyright 2023 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

#pragma once

#include <glib-object.h>
#include <libipuz/ipuz-clue.h>

G_BEGIN_DECLS


typedef struct _IPuzClueSets IPuzClueSets;
#define IPUZ_TYPE_CLUE_SETS (ipuz_clue_sets_get_type ())


typedef void (*IPuzClueSetsForeachFunc) (IPuzClueSets            *clue_sets,
                                         IPuzClueDirection        direction,
                                         gpointer                 user_data);


GType              ipuz_clue_sets_get_type               (void);
IPuzClueSets      *ipuz_clue_sets_new                    (void);
IPuzClueSets      *ipuz_clue_sets_ref                    (IPuzClueSets            *clue_sets);
void               ipuz_clue_sets_unref                  (IPuzClueSets            *clue_sets);
gboolean           ipuz_clue_sets_equal                  (IPuzClueSets            *a,
                                                          IPuzClueSets            *b);
void               ipuz_clue_sets_clone                  (IPuzClueSets            *src,
                                                          IPuzClueSets            *dest);
IPuzClueDirection  ipuz_clue_sets_add_set                (IPuzClueSets            *clue_sets,
                                                          IPuzClueDirection        direction,
                                                          const gchar             *label);
guint              ipuz_clue_sets_get_n_clue_sets        (IPuzClueSets            *clue_sets);
void               ipuz_clue_sets_foreach                (IPuzClueSets            *clue_sets,
                                                          IPuzClueSetsForeachFunc  func,
                                                          gpointer                 user_data);
void               ipuz_clue_sets_append_clue            (IPuzClueSets            *clue_sets,
                                                          IPuzClueDirection        direction,
                                                          IPuzClue                *clue);
void               ipuz_clue_sets_remove_clue            (IPuzClueSets            *clue_sets,
                                                          IPuzClueDirection        direction,
                                                          IPuzClue                *clue,
                                                          gboolean                 remove_empty);
IPuzClueDirection  ipuz_clue_sets_get_direction          (IPuzClueSets            *clue_sets,
                                                          guint                    index);
const gchar       *ipuz_clue_sets_get_label              (IPuzClueSets            *clue_sets,
                                                          IPuzClueDirection        direction);
GArray            *ipuz_clue_sets_get_clues              (IPuzClueSets            *clue_sets,
                                                          IPuzClueDirection        direction);
IPuzClueDirection  ipuz_clue_sets_get_original_direction (IPuzClueSets            *clue_sets,
                                                          IPuzClueDirection        direction);
void               ipuz_clue_sets_unlink_direction       (IPuzClueSets            *clue_sets,
                                                          IPuzClueDirection        direction);


G_DEFINE_AUTOPTR_CLEANUP_FUNC(IPuzClueSets, ipuz_clue_sets_unref);


G_END_DECLS
