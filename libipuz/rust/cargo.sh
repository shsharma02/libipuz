#!/bin/sh

set -eux

export OUTPUT="$2"
export CARGO_TARGET_DIR="$3"/target
export CARGO_HOME="$CARGO_TARGET_DIR"/cargo-home
export BUILD_TYPE="$4"

CARGO_TOML=$1/Cargo.toml

if [ "$BUILD_TYPE" = "release" ]
then
    echo "RELEASE MODE"
    cargo build --manifest-path $CARGO_TOML --release
    cp "$CARGO_TARGET_DIR"/release/libipuz_rust.a "$OUTPUT"
else
    echo "DEBUG MODE"
    cargo build --manifest-path $CARGO_TOML
    cp "$CARGO_TARGET_DIR"/debug/libipuz_rust.a "$OUTPUT"
fi
