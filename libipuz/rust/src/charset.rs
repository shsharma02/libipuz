use glib::translate::*;
use glib_sys::gboolean;
use libc::c_char;
use libc::c_int;
use libc::c_uint;

use std::collections::HashMap;
use std::ffi::CStr;
use std::mem::ManuallyDrop;
use std::sync::Arc;

/// A mutable histogram of character counts.
///
/// To create an empty charset, use `CharsetBuilder::default()`.
#[derive(Default)]
pub struct CharsetBuilder {
    histogram: HashMap<char, u32>,
}

/// Data about a character in a `Charset`.  The "value" in a key/value pair where the "key" is a character.
#[derive(PartialEq)]
struct CharsetEntry {
    /// Index of the character within the `Charset`'s ordered version.
    index: u32,

    /// How many of this character in the histogram.
    count: u32,
}

/// An immutable histogram, compiled from a `CharsetBuilder`.
#[derive(PartialEq)]
pub struct Charset {
    /// Histogram of characters and their counts plus derived values.
    histogram: HashMap<char, CharsetEntry>,

    /// All the characters in the histogram, but in order.
    ordered: String,

    /// Sum of all the counts of all the characters.
    sum_of_counts: usize,
}

impl CharsetBuilder {
    /// Adds `text`'s character counts to the histogram.
    fn add_text(&mut self, text: &str) {
        for ch in text.chars() {
            self.add_character(ch);
        }
    }

    /// Adds a single character to the histogram.
    fn add_character(&mut self, ch: char) {
        self.histogram
            .entry(ch)
            .and_modify(|e| *e += 1)
            .or_insert(1);
    }

    /// Tries to remove each character in `text` from the histogram, and returns whether this succeeds.
    ///
    /// For example, if the histogram is for `HELLOWORLD`, removing `HELLO` will succeed and the
    /// histogram will be left with one of each character in `WORLD`; the function will return `true`.
    ///
    /// However, removing `FOOBAR` from `HELLOWORLD` would fail, since that string doesn't contain `F`
    /// or `B`; the function would return `false` and the `CharsetBuilder` would remain unchanged.
    fn remove_text(&mut self, text: &str) -> bool {
        // First, copy our histogram

        let mut histogram = self.histogram.clone();

        // Try decrementing the counts until we finish or until we hit
        // a key with count=0 - in the latter case; the text can't be
        // removed.

        for ch in text.chars() {
            if let Some(v) = histogram.get_mut(&ch) {
                match *v {
                    2.. => *v -= 1,
                    1 => {
                        let _ = histogram.remove(&ch);
                    }
                    0 => return false, // can't remove any more of the current ch; we are done
                }
            } else {
                // no entry for the current ch; we are done
                return false;
            }
        }

        // Replace with the new counts
        self.histogram = histogram;

        true
    }

    /// Consumes the builder and returns an immutable `Charset`.
    ///
    /// The `Charset` can then be queried for character counts or iterated upon.
    fn build(self) -> Charset {
        struct Char {
            ch: char,
            count: u32,
        }

        let CharsetBuilder { mut histogram } = self;

        let mut characters: Vec<Char> = histogram
            .drain()
            .map(|(ch, count)| Char { ch, count })
            .collect();
        characters.sort_by(|a, b| a.ch.cmp(&b.ch));

        let mut histogram = HashMap::new();
        let mut ordered = String::with_capacity(characters.len());
        let mut sum_of_counts = 0;

        for (index, c) in characters.drain(..).enumerate() {
            ordered.push(c.ch);
            sum_of_counts += c.count as usize;

            histogram.insert(
                c.ch,
                CharsetEntry {
                    index: index as u32,
                    count: c.count,
                },
            );
        }

        Charset {
            histogram,
            ordered,
            sum_of_counts,
        }
    }
}

impl Charset {
    /// Gets the index of `ch` within the sorted characters of the histogram, as `Some(index)`.
    ///
    /// If `ch` does not exist in the `Charset`, returns `None`.
    fn get_char_index(&self, ch: char) -> Option<u32> {
        self.histogram.get(&ch).map(|e| e.index)
    }

    /// Gets the count of a certain `ch` in the histogram, as `Some<(count)`.
    ///
    /// If `ch` does not exist in the `Charset`, returns `None`.
    fn get_char_count(&self, ch: char) -> Option<u32> {
        self.histogram.get(&ch).map(|e| e.count)
    }

    /// Returns how many distinct characters there are in the histogram.
    fn get_n_chars(&self) -> usize {
        self.histogram.len()
    }

    /// Returns how many total characters there are in the histogram (e.g. the sum of all counts).
    fn get_size(&self) -> usize {
        self.sum_of_counts
    }

    /// Returns a list of sorted distinct characters in the histogram.
    fn serialize(&self) -> String {
        self.ordered.clone()
    }

    /// Constructs a `Charset` from the characters in a string.
    fn deserialize(s: &str) -> Self {
        let mut builder = CharsetBuilder::default();
        builder.add_text(s);
        builder.build()
    }

    /// Creates an iterator that will yield `CharsetIterValue` for each character in the histogram.
    fn iter(&self) -> CharsetIter {
        CharsetIter {
            charset: self,
            ordered_iter: self.ordered.chars(),
        }
    }
}

/// Iterator returned by [`Charset::iter()`].
pub struct CharsetIter<'a> {
    charset: &'a Charset,
    ordered_iter: std::str::Chars<'a>,
}

/// Value yielded by [`CharsetIter`].
pub struct CharsetIterValue {
    /// The character in the histogram.
    pub c: char,

    /// Count of `c` in the histogram.
    pub count: u32,
}

/* Keep in sync with ipuz-charset.h */
#[repr(C)]
#[derive(Copy, Clone, Default)]
pub struct IPuzCharsetIterValue {
    c: u32,
    count: c_uint,
}

impl<'a> Iterator for CharsetIter<'a> {
    type Item = CharsetIterValue;

    fn next(&mut self) -> Option<Self::Item> {
        self.ordered_iter.next().map(|c| CharsetIterValue {
            c,
            count: self.charset.get_char_count(c).unwrap(), // we always query for characters that exist, so we can unwrap
        })
    }
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_charset_builder_new() -> Box<CharsetBuilder> {
    Box::new(CharsetBuilder::default())
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_charset_builder_new_from_text(
    text: *const c_char,
) -> *mut CharsetBuilder {
    ipuz_return_val_if_fail! {
        ipuz_charset_builder_new_from_text => std::ptr::null_mut();

        !text.is_null(),
    }

    let mut builder = CharsetBuilder::default();

    let text = CStr::from_ptr(text).to_str().unwrap();
    builder.add_text(&text);

    Box::into_raw(Box::new(builder))
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_charset_builder_new_for_language(
    lang: *const c_char,
) -> Option<Box<CharsetBuilder>> {
    const LANGUAGES: [(&str, &str); 5] = [
        ("C", "ABCDEFGHIJKLMNOPQRSTUVWXYZ"),
        ("en", "ABCDEFGHIJKLMNOPQRSTUVWXYZ"),
        ("es", "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ"),
        ("nl", "ABCDEFGHIJKLMNOPQRSTUVWXYZ"),
        ("it", "ABCDEFGHILMNOPQRSTUVZ"),
    ];

    let lang = CStr::from_ptr(lang).to_str().unwrap();
    for (code, alphabet) in LANGUAGES {
        if code.eq_ignore_ascii_case(lang) {
            let mut builder = CharsetBuilder::default();
            builder.add_text(alphabet);
            return Some(Box::new(builder));
        }
    }

    None
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_charset_builder_build(
    builder: *mut CharsetBuilder,
) -> *const Charset {
    ipuz_return_val_if_fail! {
        ipuz_charset_builder_build => std::ptr::null();

        !builder.is_null(),
    }

    let builder = Box::from_raw(builder);
    let charset = builder.build();
    Arc::into_raw(Arc::new(charset))
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_charset_builder_add_text(
    builder: *mut CharsetBuilder,
    text: *const c_char,
) {
    ipuz_return_if_fail! {
        ipuz_charset_builder_add_text;

        !builder.is_null(),
        !text.is_null(),
    }

    let builder = &mut *builder;
    let text = CStr::from_ptr(text).to_str().unwrap();
    builder.add_text(text);
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_charset_builder_add_character(
    builder: *mut CharsetBuilder, ch: u32
) {
    ipuz_return_if_fail! {
        ipuz_charset_builder_add_character;

        !builder.is_null(),
    }
    let builder = &mut *builder;
    let ch = char::from_u32(ch).unwrap();
    builder.add_character(ch);
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_charset_builder_remove_text(
    builder: *mut CharsetBuilder,
    text: *const c_char,
) -> gboolean {
    ipuz_return_val_if_fail! {
        ipuz_charset_builder_remove_text => false.into_glib();

        !builder.is_null(),
        !text.is_null(),
    }

    let builder = &mut *builder;
    let text = CStr::from_ptr(text).to_str().unwrap();
    builder.remove_text(text).into_glib()
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_charset_ref(charset: *const Charset) -> *const Charset {
    Arc::increment_strong_count(charset);
    charset
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_charset_unref(charset: *const Charset) {
    Arc::decrement_strong_count(charset);
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_charset_compare(a: *const Charset, b: *const Charset) -> gboolean {
    ipuz_return_val_if_fail! {
        ipuz_charset_compare => false.into_glib();

        !a.is_null(),
        !b.is_null(),
    }

    let a = ManuallyDrop::new(Arc::from_raw(a));
    let b = ManuallyDrop::new(Arc::from_raw(b));

    (*a == *b).into_glib()
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_charset_get_n_chars(charset: *const Charset) -> usize {
    ipuz_return_val_if_fail! {
        ipuz_charset_get_n_chars => 0usize;

        !charset.is_null(),
    }

    let charset = ManuallyDrop::new(Arc::from_raw(charset));
    charset.get_n_chars()
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_charset_get_size(charset: *const Charset) -> usize {
    ipuz_return_val_if_fail! {
        ipuz_charset_get_size => 0;

        !charset.is_null(),
    }

    let charset = ManuallyDrop::new(Arc::from_raw(charset));
    charset.get_size()
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_charset_get_char_index(charset: *const Charset, ch: u32) -> c_int {
    if charset.is_null() {
        return 0;
    }

    let charset = ManuallyDrop::new(Arc::from_raw(charset));
    let ch = char::from_u32(ch).unwrap();
    charset.get_char_index(ch).map(|c| c as i32).unwrap_or(-1)
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_charset_get_char_count(charset: *const Charset, ch: u32) -> c_uint {
    ipuz_return_val_if_fail! {
        ipuz_charset_get_char_count => 0;

        !charset.is_null(),
    }

    let charset = ManuallyDrop::new(Arc::from_raw(charset));
    let ch = char::from_u32(ch).unwrap();
    charset.get_char_count(ch).unwrap_or(0)
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_charset_serialize(charset: *const Charset) -> *mut c_char {
    ipuz_return_val_if_fail! {
        ipuz_charset_serialize => std::ptr::null_mut();

        !charset.is_null(),
    }

    let charset = ManuallyDrop::new(Arc::from_raw(charset));
    charset.serialize().to_glib_full()
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_charset_deserialize(text: *const c_char) -> *const Charset {
    ipuz_return_val_if_fail! {
        ipuz_charset_deserialize => std::ptr::null();

        !text.is_null(),
    }

    let text = CStr::from_ptr(text).to_str().unwrap();

    let charset = Charset::deserialize(text);
    Arc::into_raw(Arc::new(charset))
}

pub struct IPuzCharsetIter {
    items: Vec<IPuzCharsetIterValue>,
    current_item_index: usize,
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_charset_iter_first(charset: *const Charset) -> *mut IPuzCharsetIter {
    ipuz_return_val_if_fail! {
        ipuz_charset_iter_first => std::ptr::null_mut();

        !charset.is_null(),
    }

    let charset = ManuallyDrop::new(Arc::from_raw(charset));
    let items: Vec<_> = charset
        .iter()
        .map(|v| IPuzCharsetIterValue {
            c: v.c as u32,
            count: v.count,
        })
        .collect();

    if items.len() == 0 {
        std::ptr::null_mut()
    } else {
        Box::into_raw(Box::new(IPuzCharsetIter {
            items,
            current_item_index: 0,
        }))
    }
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_charset_iter_next(
    iter: *mut IPuzCharsetIter,
) -> *mut IPuzCharsetIter {
    ipuz_return_val_if_fail! {
        ipuz_charset_iter_next => std::ptr::null_mut();

        !iter.is_null(),
    }

    let ipuz_iter = &mut *iter;

    ipuz_iter.current_item_index += 1;

    if ipuz_iter.current_item_index == ipuz_iter.items.len() {
        // we are done; free the iter and tell the caller that there's nothing left
        let _ = Box::from_raw(iter);
        std::ptr::null_mut()
    } else {
        // caller keeps on using this iter
        iter
    }
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_charset_iter_get_value(
    iter: *mut IPuzCharsetIter,
) -> IPuzCharsetIterValue {
    ipuz_return_val_if_fail! {
        ipuz_charset_iter_get_value => IPuzCharsetIterValue::default();

        !iter.is_null(),
    }

    let iter = &mut *iter;
    iter.items[iter.current_item_index]
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn handles_ascii() {
        let mut builder = CharsetBuilder::default();
        builder.add_text("AAABCDEEEFGHIIIIIIJKLMNOPQRSTUVWXYZZZZ");

        let charset = builder.build();
        assert_eq!(charset.get_n_chars(), 26);

        assert_eq!(charset.get_char_index('A').unwrap(), 0);
        assert_eq!(charset.get_char_index('M').unwrap(), 12);
        assert_eq!(charset.get_char_index('Z').unwrap(), 25);

        assert!(charset.get_char_index('7').is_none());
    }

    #[test]
    fn handles_non_ascii() {
        let mut builder = CharsetBuilder::default();
        builder.add_text("ÁRBOL");
        builder.add_text("BLÅHAJ");
        builder.add_text("BORLA");
        builder.add_text("TRALALA");

        let charset = builder.build();
        assert_eq!(charset.get_n_chars(), 10);

        let serialized = charset.serialize();
        assert_eq!(serialized.as_str(), "ABHJLORTÁÅ");

        assert!(charset.get_char_index('X').is_none());
    }

    #[test]
    fn serialization_roundtrip() {
        let mut builder = CharsetBuilder::default();
        builder.add_text("ABCDEFGHIJKLMNOPQRSTUVWXYZÁÉÍÓÚ");
        let charset = builder.build();

        let serialized = charset.serialize();
        let deserialized = Charset::deserialize(&serialized);

        let roundtrip = deserialized.serialize();
        assert_eq!(serialized, roundtrip);
        assert_eq!(serialized, "ABCDEFGHIJKLMNOPQRSTUVWXYZÁÉÍÓÚ");
    }

    #[test]
    fn supports_histogram() {
        let mut builder = CharsetBuilder::default();

        let the_string = "ABBCCCDDDDEEEEEFFFFFFGGGGGGG";
        builder.add_text(the_string);
        let charset = builder.build();

        assert_eq!(charset.get_size(), the_string.len());

        assert_eq!(charset.get_char_count('A').unwrap(), 1);
        assert_eq!(charset.get_char_count('B').unwrap(), 2);
        assert_eq!(charset.get_char_count('C').unwrap(), 3);
        assert_eq!(charset.get_char_count('D').unwrap(), 4);
        assert_eq!(charset.get_char_count('E').unwrap(), 5);
        assert_eq!(charset.get_char_count('F').unwrap(), 6);
        assert_eq!(charset.get_char_count('G').unwrap(), 7);

        assert!(charset.get_char_count('H').is_none());
    }

    #[test]
    fn remove_text_fails() {
        let mut builder = CharsetBuilder::default();
        builder.add_text("AABBCCDDEEFFG");

        assert_eq!(builder.remove_text("TEXT"), false);

        let charset = builder.build();
        assert_eq!(charset.get_char_count('A').unwrap(), 2);
        assert_eq!(charset.get_char_count('B').unwrap(), 2);
        assert_eq!(charset.get_char_count('C').unwrap(), 2);
        assert_eq!(charset.get_char_count('D').unwrap(), 2);
        assert_eq!(charset.get_char_count('E').unwrap(), 2);
        assert_eq!(charset.get_char_count('F').unwrap(), 2);
        assert_eq!(charset.get_char_count('G').unwrap(), 1);
    }

    #[test]
    fn remove_text_succeeds() {
        let mut builder = CharsetBuilder::default();
        builder.add_text("AABBCCDDEEFFG");

        assert_eq!(builder.remove_text("ABCDEFG"), true);

        let charset = builder.build();
        assert_eq!(charset.get_char_count('A').unwrap(), 1);
        assert_eq!(charset.get_char_count('B').unwrap(), 1);
        assert_eq!(charset.get_char_count('C').unwrap(), 1);
        assert_eq!(charset.get_char_count('D').unwrap(), 1);
        assert_eq!(charset.get_char_count('E').unwrap(), 1);
        assert_eq!(charset.get_char_count('F').unwrap(), 1);
        assert!(charset.get_char_count('G').is_none());
    }
}
