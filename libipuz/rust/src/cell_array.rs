use std::ffi::c_uint;

// Defining IPuzCellCoord Struct
#[repr(C)]
#[derive(Copy, Clone, Eq, Ord, PartialEq, PartialOrd)]
pub struct IPuzCellCoord {
    row: c_uint,
    column: c_uint,
}

// Defining IPuzCoordArray Struct
#[derive(Clone)]
pub struct IPuzCoordArray {
    data: Vec<IPuzCellCoord>,
}

impl IPuzCoordArray {
    pub fn new() -> Self {
        IPuzCoordArray { data: Vec::new() }
    }

    pub fn add(&mut self, coord: IPuzCellCoord) {
        if !self.find(&coord) {
            self.data.push(coord);
            self.data.sort();
        }
    }

    pub fn remove(&mut self, coord: IPuzCellCoord) {
        if let Some(index) = self.data.iter().position(|&c| c == coord) {
            self.data.remove(index);
        }
    }

    pub fn toggle(&mut self, coord: IPuzCellCoord) {
        if let Some(index) = self.data.iter().position(|&c| c == coord) {
            self.data.remove(index);
        } else {
            self.data.push(coord);
            self.data.sort();
        }
    }

    pub fn find(&self, coord: &IPuzCellCoord) -> bool {
        self.data.contains(coord)
    }

    pub fn len(&self) -> usize {
        self.data.len()
    }

    pub fn index(&self, i: usize) -> Option<&IPuzCellCoord> {
        self.data.get(i)
    }

    pub fn print(&self) {
        print!("[");
        for (i, coord) in self.data.iter().enumerate() {
            if i > 0 {
                print!(" ");
            }
            print!("[{}, {}]", coord.row, coord.column);
            if i != self.data.len() - 1 {
                print!("\n");
            }
        }
        print!("]\n");
    }

    pub fn shuffle(&mut self) {
        use rand::seq::SliceRandom;
        let mut rng = rand::thread_rng();
        self.data.shuffle(&mut rng);
    }

    pub fn pop_front(&mut self) -> Option<IPuzCellCoord> {
        if !self.data.is_empty() {
            Some(self.data.remove(0))
        } else {
            None
        }
    }
}
