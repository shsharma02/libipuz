#include <glib.h>
#include <libipuz/libipuz.h>
#include <locale.h>


static void
test_acrostic_load (void)
{
  g_autoptr (IPuzPuzzle) puzzle = NULL;
  GError *error = NULL;
  gchar *path;

  setlocale(LC_ALL, "en_US.utf8");

  path = g_test_build_filename (G_TEST_DIST, "acrostic1.ipuz", NULL);
  puzzle = ipuz_puzzle_new_from_file (path, &error);
  g_free (path);

  if (error != NULL)
    {
      g_print ("Error: %s\n", error->message);
    }
  ipuz_crossword_print (IPUZ_CROSSWORD (puzzle));
  g_assert (puzzle != NULL);
}

static void
test_acrostic_load2 (void)
{
  g_autoptr (IPuzPuzzle) puzzle = NULL;
  GError *error = NULL;
  gchar *path;

  setlocale(LC_ALL, "en_US.utf8");

  path = g_test_build_filename (G_TEST_DIST, "acrostic2.ipuz", NULL);
  puzzle = ipuz_puzzle_new_from_file (path, &error);
  g_free (path);

  if (error != NULL)
    {
      g_print ("Error: %s\n", error->message);
    }

  g_assert (puzzle != NULL);
}

static void
test_acrostic_quote_clue (void)
{
  g_autoptr (IPuzPuzzle) puzzle_a = NULL;
  g_autoptr (IPuzPuzzle) puzzle_b = NULL;
  IPuzClue *clue_a;
  IPuzClue *clue_b;
  GError *error = NULL;
  gchar *path;

  setlocale(LC_ALL, "en_US.utf8");

  /* Load file with quote clue */
  path = g_test_build_filename (G_TEST_DIST, "acrostic2.ipuz", NULL);
  puzzle_a = ipuz_puzzle_new_from_file (path, &error);
  g_assert (error == NULL);
  g_free (path);

  /* same file without quote clue */
  path = g_test_build_filename (G_TEST_DIST, "acrostic2-no-quote-clue.ipuz", NULL);
  puzzle_b = ipuz_puzzle_new_from_file (path, &error);
  g_assert (error == NULL);
  g_free (path);

  clue_a = ipuz_acrostic_get_quote_clue (IPUZ_ACROSTIC (puzzle_a));
  clue_b = ipuz_acrostic_get_quote_clue (IPUZ_ACROSTIC (puzzle_b));

  /* These clues will have different values of clue_set, so we can't
   * just compare that they're equal. We do want to show that they
   * have the same cells, though */
  g_assert (clue_a != NULL);
  g_assert (clue_b != NULL);
  g_assert (clue_a->cells != NULL);
  g_assert (clue_b->cells != NULL);
  g_assert (clue_a->cells->len == clue_b->cells->len);

  g_assert (memcmp (clue_a->cells->data,
                    clue_b->cells->data,
                    clue_a->cells->len * sizeof (IPuzCellCoord)) == 0);
}

static void
test_acrostic_save (void)
{
  g_autoptr (IPuzPuzzle) puzzle_a = NULL;
  g_autoptr (IPuzPuzzle) puzzle_b = NULL;
  GError *error = NULL;
  gchar *path;
  g_autofree gchar *data = NULL;
  gsize length;

  setlocale(LC_ALL, "en_US.utf8");

  path = g_test_build_filename (G_TEST_DIST, "acrostic1.ipuz", NULL);
  puzzle_a = ipuz_puzzle_new_from_file (path, &error);
  g_assert (error == NULL);
  g_free (path);

  data = ipuz_puzzle_save_to_data (puzzle_a, &length);
  g_print ("%s", data);
  puzzle_b = ipuz_puzzle_new_from_data (data, length, &error);
  g_assert (error == NULL);

  g_assert (ipuz_puzzle_equal (puzzle_a, puzzle_b));
}

static void
test_acrostic_deep_copy (void)
{
  g_autoptr (IPuzPuzzle) puzzle_a = NULL;
  g_autoptr (IPuzPuzzle) puzzle_b = NULL;
  GError *error = NULL;
  gchar *path;

  setlocale(LC_ALL, "en_US.utf8");

  path = g_test_build_filename (G_TEST_DIST, "acrostic1.ipuz", NULL);
  puzzle_a = ipuz_puzzle_new_from_file (path, &error);
  g_assert (error == NULL);
  g_free (path);

  puzzle_b = ipuz_puzzle_deep_copy (puzzle_a);

  g_assert (ipuz_puzzle_equal (puzzle_a, puzzle_b));
}

static void
test_acrostic_quote_str (void)
{
  g_autoptr (IPuzPuzzle) puzzle = NULL;

  puzzle = ipuz_acrostic_new ();

  g_assert_cmpstr (ipuz_acrostic_get_quote_str (IPUZ_ACROSTIC (puzzle)), ==, NULL);

  ipuz_acrostic_set_quote_str (IPUZ_ACROSTIC (puzzle), "QUOTE STRING");

  g_assert_cmpstr (ipuz_acrostic_get_quote_str (IPUZ_ACROSTIC (puzzle)), ==, "QUOTE STRING");

  ipuz_acrostic_set_quote_str (IPUZ_ACROSTIC (puzzle), "  Some Garbage#$");

  g_assert_cmpstr (ipuz_acrostic_get_quote_str (IPUZ_ACROSTIC (puzzle)), ==, "SOME GARBAGE");

  ipuz_acrostic_set_quote_str (IPUZ_ACROSTIC (puzzle), "  multi  space");

  g_assert_cmpstr (ipuz_acrostic_get_quote_str (IPUZ_ACROSTIC (puzzle)), ==, "MULTI  SPACE");
}

static void
test_acrostic_sync_quote_str_to_grid (void)
{
  g_autoptr (IPuzPuzzle) puzzle = NULL;
  IPuzCell *cell = NULL;
  IPuzCellCoord coord = {
    .row = 0,
    .column = 0,
  };

  puzzle = ipuz_acrostic_new ();

  ipuz_acrostic_set_quote_str (IPUZ_ACROSTIC (puzzle), "QUOTE STRING");

  /* sets board size to 4 * 3 */
  /*
   *  +---+---+---+---+
   *  | Q | U | O | T |
   *  +---+---+---+---+
   *  | E | # | S | T |
   *  +---+---+---+---+
   *  | R | I | N | G |
   *  +---+---+---+---+
   *
   */

  ipuz_crossword_fix_all (IPUZ_CROSSWORD (puzzle),
                          "sync-direction", IPUZ_ACROSTIC_SYNC_QUOTE_STR_TO_GRID,
                          NULL);

  g_assert_cmpint (ipuz_crossword_get_width (IPUZ_CROSSWORD (puzzle)), ==, 4);
  g_assert_cmpint (ipuz_crossword_get_height (IPUZ_CROSSWORD (puzzle)), ==, 3);

  cell = ipuz_crossword_get_cell (IPUZ_CROSSWORD (puzzle), coord);
  g_assert_cmpstr (ipuz_cell_get_solution (cell), ==, "Q");
  g_assert_cmpint (ipuz_cell_get_cell_type (cell), ==, IPUZ_CELL_NORMAL);

  coord.row = 1;
  coord.column = 0;
  cell = ipuz_crossword_get_cell (IPUZ_CROSSWORD (puzzle), coord);
  g_assert_cmpstr (ipuz_cell_get_solution (cell), ==, "E");
  g_assert_cmpint (ipuz_cell_get_cell_type (cell), ==, IPUZ_CELL_NORMAL);

  coord.row = 1;
  coord.column = 1;
  cell = ipuz_crossword_get_cell (IPUZ_CROSSWORD (puzzle), coord);
  g_assert_cmpstr (ipuz_cell_get_solution (cell), ==, NULL);
  g_assert_cmpint (ipuz_cell_get_cell_type (cell), ==, IPUZ_CELL_BLOCK);

  coord.row = 2;
  coord.column = 3;
  cell = ipuz_crossword_get_cell (IPUZ_CROSSWORD (puzzle), coord);
  g_assert_cmpstr (ipuz_cell_get_solution (cell), ==, "G");
  g_assert_cmpint (ipuz_cell_get_cell_type (cell), ==, IPUZ_CELL_NORMAL);
}

static void
test_acrostic_sync_grid_to_quote_str (void)
{
  g_autoptr (IPuzPuzzle) puzzle = NULL;
  IPuzCell *cell = NULL;
  IPuzCellCoord coord = {
    .row = 0,
    .column = 2,
  };

  puzzle = ipuz_acrostic_new ();

  ipuz_acrostic_set_quote_str (IPUZ_ACROSTIC (puzzle), "QUOTE STRING");

  /* sets board size to 4 * 3 */
  /*
   *  +---+---+---+---+
   *  | Q | U | O | T |
   *  +---+---+---+---+
   *  | E | # | S | T |
   *  +---+---+---+---+
   *  | R | I | N | G |
   *  +---+---+---+---+
   *
   */

  ipuz_crossword_fix_all (IPUZ_CROSSWORD (puzzle),
                          "sync-direction", IPUZ_ACROSTIC_SYNC_QUOTE_STR_TO_GRID,
                          NULL);

  cell = ipuz_crossword_get_cell (IPUZ_CROSSWORD (puzzle), coord);
  ipuz_cell_set_solution (cell, "I");

  coord.row = 2;
  coord.column = 0;
  cell = ipuz_crossword_get_cell (IPUZ_CROSSWORD (puzzle), coord);
  ipuz_cell_set_solution (cell, "U");

  coord.row = 2;
  coord.column = 1;
  cell = ipuz_crossword_get_cell (IPUZ_CROSSWORD (puzzle), coord);
  ipuz_cell_set_solution (cell, "N");

  coord.row = 2;
  coord.column = 2;
  cell = ipuz_crossword_get_cell (IPUZ_CROSSWORD (puzzle), coord);
  ipuz_cell_set_solution (cell, "G");

  coord.row = 2;
  coord.column = 3;
  cell = ipuz_crossword_get_cell (IPUZ_CROSSWORD (puzzle), coord);
  ipuz_cell_set_cell_type (cell, IPUZ_CELL_BLOCK);


  ipuz_crossword_fix_all (IPUZ_CROSSWORD (puzzle),
                          "sync-direction", IPUZ_ACROSTIC_SYNC_GRID_TO_QUOTE_STR,
                          NULL);

  g_assert_cmpstr (ipuz_acrostic_get_quote_str (IPUZ_ACROSTIC (puzzle)), ==, "QUITE STUNG");

}

static void
test_acrostic_update_quote_str (void)
{
  IPuzCellCoord coord = {
    .row = 0,
    .column = 0,
  };
  IPuzCell *cell;

  g_autoptr (IPuzPuzzle) puzzle = NULL;
  g_autofree gchar *quote_str = NULL;

  puzzle = ipuz_acrostic_new ();
  ipuz_acrostic_set_quote_str (IPUZ_ACROSTIC (puzzle), "  quote string       ");
  ipuz_acrostic_fix_quote_str (IPUZ_ACROSTIC (puzzle), IPUZ_ACROSTIC_SYNC_QUOTE_STR_TO_GRID);
  cell = ipuz_crossword_get_cell (IPUZ_CROSSWORD (puzzle), coord);
  g_assert_cmpstr (ipuz_cell_get_solution (cell), ==, "Q");

  /* Set a new quote str and make sure it updates corectly */
  ipuz_acrostic_set_quote_str (IPUZ_ACROSTIC (puzzle), " longer quote #string  ");
  ipuz_acrostic_fix_quote_str (IPUZ_ACROSTIC (puzzle), IPUZ_ACROSTIC_SYNC_QUOTE_STR_TO_GRID);
  g_assert_cmpint (ipuz_crossword_get_width (IPUZ_CROSSWORD (puzzle)), ==, 5);
  g_assert_cmpint (ipuz_crossword_get_height (IPUZ_CROSSWORD (puzzle)), ==, 4);

  cell = ipuz_crossword_get_cell (IPUZ_CROSSWORD (puzzle), coord);
  g_assert_cmpstr (ipuz_cell_get_solution (cell), ==, "L");

}

static void
test_acrostic_non_english (void)
{
  g_autoptr (IPuzPuzzle) puzzle = NULL;
  IPuzCharsetBuilder *builder;
  IPuzCharset *charset;
  gchar *quote_str = NULL;

  puzzle = ipuz_acrostic_new ();

  /* Test for ES */
  builder = ipuz_charset_builder_new_for_language ("es");
  charset = ipuz_charset_builder_build (builder);
  builder = NULL;
  g_object_set (puzzle, "lang-charset", charset, NULL);

  ipuz_acrostic_set_quote_str (IPUZ_ACROSTIC (puzzle), "QUOTE STRIÑG");

  g_assert_cmpstr (ipuz_acrostic_get_quote_str (IPUZ_ACROSTIC (puzzle)), ==, "QUOTE STRIÑG");

  ipuz_acrostic_fix_quote_str (IPUZ_ACROSTIC (puzzle), IPUZ_ACROSTIC_SYNC_QUOTE_STR_TO_GRID);

  quote_str = g_strdup (ipuz_acrostic_get_quote_str (IPUZ_ACROSTIC (puzzle)));

  ipuz_acrostic_fix_quote_str (IPUZ_ACROSTIC (puzzle), IPUZ_ACROSTIC_SYNC_GRID_TO_QUOTE_STR);

  g_assert_cmpstr (quote_str, ==, ipuz_acrostic_get_quote_str (IPUZ_ACROSTIC (puzzle)));

  g_free (quote_str);

  /* Test for IT */
  builder = ipuz_charset_builder_new_for_language ("it");
  charset = ipuz_charset_builder_build (builder);
  builder = NULL;
  g_object_set (puzzle, "lang-charset", charset, NULL);

  ipuz_acrostic_set_quote_str (IPUZ_ACROSTIC (puzzle), "ABCDEFGHIJKLMNOPQRSTUVWXYZ");

  g_assert_cmpstr (ipuz_acrostic_get_quote_str (IPUZ_ACROSTIC (puzzle)), ==, "ABCDEFGHI  LMNOPQRSTUV   Z");

  ipuz_acrostic_fix_quote_str (IPUZ_ACROSTIC (puzzle), IPUZ_ACROSTIC_SYNC_GRID_TO_QUOTE_STR);

  quote_str = g_strdup (ipuz_acrostic_get_quote_str (IPUZ_ACROSTIC (puzzle)));

  ipuz_acrostic_fix_quote_str (IPUZ_ACROSTIC (puzzle), IPUZ_ACROSTIC_SYNC_QUOTE_STR_TO_GRID);

  g_assert_cmpstr (quote_str, ==, ipuz_acrostic_get_quote_str (IPUZ_ACROSTIC (puzzle)));

  g_free (quote_str);
}

int
main (int   argc,
      char *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/acrostic/load", test_acrostic_load);
  g_test_add_func ("/acrostic/load2", test_acrostic_load2);
  g_test_add_func ("/acrostic/quote_clue", test_acrostic_quote_clue);
  g_test_add_func ("/acrostic/save", test_acrostic_save);
  g_test_add_func ("/acrostic/deep_copy", test_acrostic_deep_copy);
  g_test_add_func ("/acrostic/quote_str", test_acrostic_quote_str);
  g_test_add_func ("/acrostic/sync_quote_str_to_grid", test_acrostic_sync_quote_str_to_grid);
  g_test_add_func ("/acrostic/sync_grid_to_quote_str", test_acrostic_sync_grid_to_quote_str);
  g_test_add_func ("/acrostic/update_quote_str", test_acrostic_update_quote_str);
  g_test_add_func ("/acrostic/non_english", test_acrostic_non_english);

  return g_test_run ();
}
