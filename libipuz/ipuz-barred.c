/* ipuz-barred.c
 *
 * Copyright 2023 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */


#include "libipuz-config.h"
#include "ipuz-private.h"
#include <libipuz/ipuz-barred.h>


#define IPUZ_STYLE_HAS_BARRED_TOP(style) (ipuz_style_get_barred(style)|IPUZ_STYLE_SIDES_TOP)
#define IPUZ_STYLE_HAS_BARRED_LEFT(style) (ipuz_style_get_barred(style)|IPUZ_STYLE_SIDES_LEFT)
#define IPUZ_STYLE_HAS_BARRED_TOP_LEFT(style) ((ipuz_style_get_barred(style)& \
                                                (IPUZ_STYLE_SIDES_TOP|IPUZ_STYLE_SIDES_LEFT)) == \
                                               (IPUZ_STYLE_SIDES_TOP|IPUZ_STYLE_SIDES_LEFT))


static void                ipuz_barred_init                 (IPuzBarred         *self);
static void                ipuz_barred_class_init           (IPuzBarredClass    *klass);
static void                ipuz_barred_fixup                (IPuzPuzzle         *puzzle);
static void                ipuz_barred_fix_styles           (IPuzCrossword      *xword);
static const gchar *const *ipuz_barred_get_kind_str         (IPuzPuzzle         *puzzle);
static gboolean            ipuz_barred_clue_continues_up    (IPuzCrossword      *xword,
                                                             IPuzCellCoord       coord);
static gboolean            ipuz_barred_clue_continues_down  (IPuzCrossword      *xword,
                                                             IPuzCellCoord       coord);
static gboolean            ipuz_barred_clue_continues_left  (IPuzCrossword      *xword,
                                                             IPuzCellCoord       coord);
static gboolean            ipuz_barred_clue_continues_right (IPuzCrossword      *xword,
                                                             IPuzCellCoord       coord);
static void                ipuz_barred_mirror_cell          (IPuzCrossword      *self,
                                                             IPuzCellCoord       src_coord,
                                                             IPuzCellCoord       dest_coord,
                                                             IPuzSymmetry        symmetry,
                                                             IPuzSymmetryOffset  symmetry_offset);
static gboolean            ipuz_barred_check_mirror         (IPuzCrossword      *xword,
                                                             IPuzCellCoord       src_coord,
                                                             IPuzCellCoord       target_coord,
                                                             IPuzSymmetry        symmetry,
                                                             IPuzSymmetryOffset  symmetry_offset);


G_DEFINE_TYPE (IPuzBarred, ipuz_barred, IPUZ_TYPE_CROSSWORD);


static void
ipuz_barred_init (IPuzBarred *self)
{
  /* Pass */
}

static void
ipuz_barred_class_init (IPuzBarredClass *klass)
{
  IPuzPuzzleClass *puzzle_class = IPUZ_PUZZLE_CLASS (klass);
  IPuzCrosswordClass *crossword_class = IPUZ_CROSSWORD_CLASS (klass);

  puzzle_class->fixup = ipuz_barred_fixup;
  puzzle_class->get_kind_str = ipuz_barred_get_kind_str;
  crossword_class->fix_styles = ipuz_barred_fix_styles;
  crossword_class->clue_continues_up = ipuz_barred_clue_continues_up;
  crossword_class->clue_continues_down = ipuz_barred_clue_continues_down;
  crossword_class->clue_continues_left = ipuz_barred_clue_continues_left;
  crossword_class->clue_continues_right = ipuz_barred_clue_continues_right;
  crossword_class->mirror_cell = ipuz_barred_mirror_cell;
  crossword_class->check_mirror = ipuz_barred_check_mirror;
}

typedef struct
{
  gboolean t_count;
  gboolean l_count;
  gboolean tl_count;
} StyleFixupTuple;

static void
fixup_style_foreach (const gchar *style_name,
                     IPuzStyle   *style,
                     gpointer     user_data)
{
  StyleFixupTuple *fixup_tuple = user_data;

  if (!g_strcmp0 (style_name, IPUZ_BARRED_STYLE_TL) &&
      (IPUZ_STYLE_HAS_BARRED_TOP_LEFT (style)))
    fixup_tuple->tl_count = TRUE;
  else if (!g_strcmp0 (style_name, IPUZ_BARRED_STYLE_T) &&
           (IPUZ_STYLE_HAS_BARRED_TOP (style)))
    fixup_tuple->t_count = TRUE;
  else if (!g_strcmp0 (style_name, IPUZ_BARRED_STYLE_L) &&
           (IPUZ_STYLE_HAS_BARRED_LEFT (style)))
    fixup_tuple->l_count = TRUE;
}

static void
ensure_styles (IPuzBarred *barred)
{
  StyleFixupTuple fixup_tuple = {0, };

  ipuz_puzzle_style_foreach (IPUZ_PUZZLE (barred), fixup_style_foreach, &fixup_tuple);

  if (! fixup_tuple.tl_count)
    {
      g_autoptr (IPuzStyle) style = NULL;

      style = ipuz_style_new ();
      ipuz_style_set_style_name (style, IPUZ_BARRED_STYLE_TL);
      ipuz_style_set_barred (style, IPUZ_STYLE_SIDES_TOP | IPUZ_STYLE_SIDES_LEFT);
      ipuz_puzzle_set_style (IPUZ_PUZZLE (barred), IPUZ_BARRED_STYLE_TL, style);
    }
  if (! fixup_tuple.t_count)
    {
      g_autoptr (IPuzStyle) style = NULL;

      style = ipuz_style_new ();
      ipuz_style_set_style_name (style, IPUZ_BARRED_STYLE_T);
      ipuz_style_set_barred (style, IPUZ_STYLE_SIDES_TOP);
      ipuz_puzzle_set_style (IPUZ_PUZZLE (barred), IPUZ_BARRED_STYLE_T, style);
    }
  if (! fixup_tuple.l_count)
    {
      g_autoptr (IPuzStyle) style = NULL;

      style = ipuz_style_new ();
      ipuz_style_set_style_name (style, IPUZ_BARRED_STYLE_L);
      ipuz_style_set_barred (style, IPUZ_STYLE_SIDES_LEFT);
      ipuz_puzzle_set_style (IPUZ_PUZZLE (barred), IPUZ_BARRED_STYLE_L, style);
    }
}

static void
ipuz_barred_fixup (IPuzPuzzle *puzzle)
{
  IPUZ_PUZZLE_CLASS (ipuz_barred_parent_class)->fixup (puzzle);

  ensure_styles (IPUZ_BARRED (puzzle));
}

static const gchar *const *
ipuz_barred_get_kind_str (IPuzPuzzle *puzzle)
{
  static const char *kind_str[] =
    {
      "http://ipuz.org/crossword#1",
      "http://libipuz.org/barred#1",
      NULL
    };

  return kind_str;
}

static gboolean
ipuz_barred_clue_continues_up (IPuzCrossword *xword,
                               IPuzCellCoord  coord)
{
  IPuzStyleSides sides;

  sides = ipuz_barred_get_cell_bars (IPUZ_BARRED (xword), coord);

  if (coord.row == 0)
    return FALSE;

  if (IPUZ_STYLE_SIDES_HAS_TOP (sides))
    return FALSE;

  return IPUZ_CROSSWORD_CLASS (ipuz_barred_parent_class)->clue_continues_up (xword, coord);
}

static gboolean
ipuz_barred_clue_continues_down (IPuzCrossword *xword,
                                 IPuzCellCoord  coord)
{
  IPuzStyleSides sides;
  guint height;

  sides = ipuz_barred_get_cell_bars (IPUZ_BARRED (xword), coord);
  height = ipuz_crossword_get_height (xword);

  if (coord.row == height - 1)
    return FALSE;

  if (IPUZ_STYLE_SIDES_HAS_BOTTOM (sides))
    return FALSE;

  return IPUZ_CROSSWORD_CLASS (ipuz_barred_parent_class)->clue_continues_down (xword, coord);
}

static gboolean
ipuz_barred_clue_continues_left (IPuzCrossword *xword,
                                 IPuzCellCoord  coord)
{
  IPuzStyleSides sides;

  sides = ipuz_barred_get_cell_bars (IPUZ_BARRED (xword), coord);

  if (coord.column == 0)
    return FALSE;

  if (IPUZ_STYLE_SIDES_HAS_LEFT (sides))
    return FALSE;

  return IPUZ_CROSSWORD_CLASS (ipuz_barred_parent_class)->clue_continues_left (xword, coord);
}

static gboolean
ipuz_barred_clue_continues_right (IPuzCrossword *xword,
                                  IPuzCellCoord  coord)
{
  IPuzStyleSides sides;
  guint width;

  sides = ipuz_barred_get_cell_bars (IPUZ_BARRED (xword), coord);
  width = ipuz_crossword_get_width (xword);

  if (coord.column == width - 1)
    return FALSE;

  if (IPUZ_STYLE_SIDES_HAS_RIGHT (sides))
    return FALSE;

  return IPUZ_CROSSWORD_CLASS (ipuz_barred_parent_class)->clue_continues_right (xword, coord);
}


/* This dense function will take sides, and rotate it according the to
   the symmetry / offset. Be careful touching it. */
static IPuzStyleSides
mirror_sides (IPuzStyleSides     sides,
              IPuzSymmetry       symmetry,
              IPuzSymmetryOffset symmetry_offset)
{
  switch (symmetry)
    {
    case IPUZ_SYMMETRY_NONE:
      return sides;
    case IPUZ_SYMMETRY_ROTATIONAL_HALF:
      return ipuz_style_sides_rotate_180 (sides);
    case IPUZ_SYMMETRY_ROTATIONAL_QUARTER:
      if (symmetry_offset == IPUZ_SYMMETRY_OFFSET_OPPOSITE)
        return ipuz_style_sides_rotate_180 (sides);
      else if (symmetry_offset == IPUZ_SYMMETRY_OFFSET_CW_ADJACENT)
        return ipuz_style_sides_rotate_rt (sides);
      else if (symmetry_offset == IPUZ_SYMMETRY_OFFSET_CCW_ADJACENT)
        return ipuz_style_sides_rotate_lt (sides);
      else
        g_assert_not_reached ();
      break;
    case IPUZ_SYMMETRY_HORIZONTAL:
      return ipuz_style_sides_flip_horiz (sides);
      break;
    case IPUZ_SYMMETRY_VERTICAL:
      return ipuz_style_sides_flip_vert (sides);
    case IPUZ_SYMMETRY_MIRRORED:
      if (symmetry_offset == IPUZ_SYMMETRY_OFFSET_OPPOSITE)
        return ipuz_style_sides_flip_vert (ipuz_style_sides_flip_horiz (sides));
      else if (symmetry_offset == IPUZ_SYMMETRY_OFFSET_CW_ADJACENT)
        return ipuz_style_sides_flip_horiz (sides);
      else if (symmetry_offset == IPUZ_SYMMETRY_OFFSET_CCW_ADJACENT)
        return ipuz_style_sides_flip_vert (sides);
      else
        g_assert_not_reached ();
      break;
    default:
      g_assert_not_reached ();
    }
}


static void
ipuz_barred_mirror_cell (IPuzCrossword      *xword,
                         IPuzCellCoord       src_coord,
                         IPuzCellCoord       dest_coord,
                         IPuzSymmetry        symmetry,
                         IPuzSymmetryOffset  symmetry_offset)
{
  IPuzStyleSides sides;
  IPuzStyleSides new_sides;

  /* Chain up to get the cell type correct */
  IPUZ_CROSSWORD_CLASS (ipuz_barred_parent_class)->mirror_cell (xword, src_coord, dest_coord, symmetry, symmetry_offset);

  sides = ipuz_barred_get_cell_bars (IPUZ_BARRED (xword), src_coord);
  new_sides = mirror_sides (sides, symmetry, symmetry_offset);

  ipuz_barred_set_cell_bars (IPUZ_BARRED (xword), dest_coord, new_sides);
}

static gboolean
ipuz_barred_check_mirror (IPuzCrossword      *xword,
                          IPuzCellCoord       src_coord,
                          IPuzCellCoord       target_coord,
                          IPuzSymmetry        symmetry,
                          IPuzSymmetryOffset  symmetry_offset)
{
  IPuzStyleSides src_sides, target_sides;

  if (!IPUZ_CROSSWORD_CLASS (ipuz_barred_parent_class)->check_mirror (xword, src_coord, target_coord, symmetry, symmetry_offset))
    return FALSE;

  src_sides = ipuz_barred_get_cell_bars (IPUZ_BARRED (xword), src_coord);
  target_sides = ipuz_barred_get_cell_bars (IPUZ_BARRED (xword), target_coord);

  return (target_sides == mirror_sides (src_sides, symmetry, symmetry_offset));
}

/**
 * Public Methods
 */

IPuzPuzzle *
ipuz_barred_new (void)
{
  IPuzPuzzle *barred;

  barred = g_object_new (IPUZ_TYPE_BARRED,
                         NULL);

  return barred;
}

/* Get the adjacent cell */
static IPuzCell *
get_adjacent_cell (IPuzBarred    *self,
                   IPuzCellCoord  coord,
                   gint           row_offset,
                   gint           col_offset)
{
  if (coord.row == 0 && row_offset < 0)
    return NULL;
  if (coord.column == 0 && col_offset < 0)
    return NULL;

  coord.row += row_offset;
  coord.column += col_offset;

  return ipuz_crossword_get_cell (IPUZ_CROSSWORD (self), coord);
}

static IPuzStyleSides
check_adjacent_cell_bars (IPuzBarred     *self,
                          IPuzCellCoord   coord,
                          gint            row_offset,
                          gint            col_offset,
                          IPuzStyleSides  side_to_check)
{
  IPuzCell *cell;
  IPuzStyle *style;
  IPuzStyleSides barred;

  cell = get_adjacent_cell (self, coord, row_offset, col_offset);
  if (cell == NULL)
    return 0;

  style = ipuz_cell_get_style (cell);
  if (style == NULL)
    return 0;

  barred = ipuz_style_get_barred (style);
  if (barred & side_to_check)
    return ipuz_style_side_opposite (side_to_check);

  return 0;
}


static IPuzStyleSides
match_side (IPuzStyleSides barred_sides,
            IPuzStyleSides side,
            IPuzSymmetry   symmetry)
{
  IPuzStyleSides opposite = ipuz_style_side_opposite (side);
  if (barred_sides & side)
    {
      if (!(barred_sides & opposite))
        barred_sides ^= opposite;
    }
  else
    {
      if (barred_sides & opposite)
        barred_sides ^= opposite;
    }
  return barred_sides;
}


/**
 * ipuz_barred_calculate_side_toggle:
 * @self: An IPuzBarred
 * @coord: The cell to be toggled
 * @side: The side to be toggled. Must not be a bitfield
 * @symmetry: The symmetry to respect
 *
 * Calculate the side of a cell after toggling one of its sides while
 * taking symmetry into account. For most of the cells on the board
 * this just toggles the side requested, but the center lines and
 * center square need to be handled separately.
 *
 * Returns:
 **/
IPuzStyleSides
ipuz_barred_calculate_side_toggle (IPuzBarred     *self,
                                   IPuzCellCoord   coord,
                                   IPuzStyleSides  side,
                                   IPuzSymmetry    symmetry)
{
  guint width, height;
  IPuzStyleSides barred_sides;

  g_return_val_if_fail (IPUZ_IS_BARRED (self), 0);
  /* assert that we only have a single side we're toggling */
  g_return_val_if_fail ((side == IPUZ_STYLE_SIDES_LEFT ||
                         side == IPUZ_STYLE_SIDES_RIGHT ||
                         side == IPUZ_STYLE_SIDES_TOP ||
                         side == IPUZ_STYLE_SIDES_BOTTOM), 0);

  barred_sides = ipuz_barred_get_cell_bars (self, coord);
  width = ipuz_crossword_get_width (IPUZ_CROSSWORD (self));
  height = ipuz_crossword_get_height (IPUZ_CROSSWORD (self));

  barred_sides ^= side;

  /* Now, check the symmetry of the three areas of interest */

  /* Vertical center line.
   *
   * We update things iff we are mirrored/horizontal and if we're
   * adjusting a left or right side.
   */
  if ((width % 2 != 0) && (coord.column == width / 2) &&
      (symmetry == IPUZ_SYMMETRY_HORIZONTAL || symmetry == IPUZ_SYMMETRY_MIRRORED) &&
      (side & (IPUZ_STYLE_SIDES_LEFT | IPUZ_STYLE_SIDES_RIGHT)))
    {
      barred_sides = match_side (barred_sides, side, symmetry);
    }

  /* Horizontal center line.
   *
   * We update things iff we are mirrored/vertical and if we're
   * adjusting a top or bottom side.
   */
  if ((height % 2 != 0) && (coord.row == height / 2) &&
      (symmetry == IPUZ_SYMMETRY_VERTICAL || symmetry == IPUZ_SYMMETRY_MIRRORED) &&
      (side & (IPUZ_STYLE_SIDES_TOP | IPUZ_STYLE_SIDES_BOTTOM)))
    {
      barred_sides = match_side (barred_sides, side, symmetry);
    }

  /* Center box
   *
   * Handled as a special case for QUARTER
   */
  if ((width % 2 != 0) && (coord.column == width / 2) &&
      (height % 2 != 0) && (coord.row == height / 2) &&
      (symmetry == IPUZ_SYMMETRY_ROTATIONAL_QUARTER))
    {
      if (side & barred_sides)
        barred_sides = (IPUZ_STYLE_SIDES_TOP_LEFT | IPUZ_STYLE_SIDES_BOTTOM_RIGHT);
      else
        barred_sides = 0;
    }

  return barred_sides;
}


IPuzStyleSides
ipuz_barred_get_cell_bars (IPuzBarred     *self,
                           IPuzCellCoord   coord)
{
  IPuzStyleSides barred_sides = 0;
  IPuzCell *cell;
  IPuzStyle *style;

  g_return_val_if_fail (IPUZ_IS_BARRED (self), 0);

  cell = ipuz_crossword_get_cell (IPUZ_CROSSWORD (self), coord);
  /* coords needs to be within the board */
  g_return_val_if_fail (cell != NULL, 0);

  style = ipuz_cell_get_style (cell);
  if (style)
    barred_sides = ipuz_style_get_barred (style);

  barred_sides |= check_adjacent_cell_bars (self, coord, 0, 1, IPUZ_STYLE_SIDES_LEFT);
  barred_sides |= check_adjacent_cell_bars (self, coord, 0, -1, IPUZ_STYLE_SIDES_RIGHT);
  barred_sides |= check_adjacent_cell_bars (self, coord, 1, 0, IPUZ_STYLE_SIDES_TOP);
  barred_sides |= check_adjacent_cell_bars (self, coord, -1, 0, IPUZ_STYLE_SIDES_BOTTOM);

  return barred_sides;
}


static void
update_cell_style (IPuzCell       *cell,
                   IPuzStyleSides  sides,
                   IPuzStyle      *barred_style,
                   const gchar    *style_name)
{
  IPuzStyle *style;

  style = ipuz_cell_get_style (cell);

  if (style)
    {
      const gchar *style_name = ipuz_style_get_style_name (style);

      if (! g_strcmp0 (style_name, IPUZ_BARRED_STYLE_T) ||
          ! g_strcmp0 (style_name, IPUZ_BARRED_STYLE_L) ||
          ! g_strcmp0 (style_name, IPUZ_BARRED_STYLE_TL))
        ipuz_cell_set_style (cell, barred_style, style_name);
      /* FIXME(style-name): This misses the case where the puzzle has
       * a named style that's not one of the internal ones. In that
       * case, it will adjust the shared style to have the new sides,
       * which is almost certainly not what's wanted */
      else
        ipuz_style_set_barred (style, sides);

    }
  else
    {
      ipuz_cell_set_style (cell, barred_style, style_name);
    }
}

/* This expects ipuz_barred_fix_styles() to have been called.
 * Returns TRUE if a cell_bar is actually changed.
 */
gboolean
ipuz_barred_set_cell_bars (IPuzBarred     *self,
                           IPuzCellCoord   coord,
                           IPuzStyleSides  sides)
{
  guint width, height;
  IPuzStyle *t_style, *l_style, *tl_style;
  IPuzCell *cell;
  IPuzStyleSides old_sides = 0;

  g_return_val_if_fail (IPUZ_IS_BARRED (self), FALSE);

  width = ipuz_crossword_get_width (IPUZ_CROSSWORD (self));
  height = ipuz_crossword_get_height (IPUZ_CROSSWORD (self));

  t_style = ipuz_puzzle_get_style (IPUZ_PUZZLE (self), IPUZ_BARRED_STYLE_T);
  l_style = ipuz_puzzle_get_style (IPUZ_PUZZLE (self), IPUZ_BARRED_STYLE_L);
  tl_style = ipuz_puzzle_get_style (IPUZ_PUZZLE (self), IPUZ_BARRED_STYLE_TL);
  g_return_val_if_fail (t_style != NULL && l_style != NULL && tl_style != NULL, FALSE);

  cell = ipuz_crossword_get_cell (IPUZ_CROSSWORD (self), coord);
  if (cell == NULL)
    return FALSE;

  old_sides = ipuz_barred_get_cell_bars (self, coord);
  if (sides == old_sides)
    return FALSE;

  /* Set the right style for us */
  if (coord.column == 0 && IPUZ_STYLE_SIDES_HAS_LEFT (sides))
    sides ^= IPUZ_STYLE_SIDES_LEFT;

  if (coord.row == 0 && IPUZ_STYLE_SIDES_HAS_TOP (sides))
    sides ^= IPUZ_STYLE_SIDES_TOP;

  if (IPUZ_STYLE_SIDES_HAS_TOP (sides) && IPUZ_STYLE_SIDES_HAS_LEFT (sides))
    update_cell_style (cell, sides, tl_style, IPUZ_BARRED_STYLE_TL);
  else if (IPUZ_STYLE_SIDES_HAS_TOP (sides))
    update_cell_style (cell, sides, t_style, IPUZ_BARRED_STYLE_T);
  else if (IPUZ_STYLE_SIDES_HAS_LEFT (sides))
    update_cell_style (cell, sides, l_style, IPUZ_BARRED_STYLE_L);
  else
    ipuz_cell_set_style (cell, NULL, NULL);

  if (((sides ^ old_sides) & IPUZ_STYLE_SIDES_RIGHT) &&
      (coord.column + 1 < width))
    {
      IPuzCellCoord right_coord = coord;
      IPuzStyleSides right_sides;

      right_coord.column++;
      right_sides = ipuz_barred_get_cell_bars (self, right_coord);
      right_sides = right_sides ^ IPUZ_STYLE_SIDES_LEFT;
      ipuz_barred_set_cell_bars (self, right_coord, right_sides);
    }

  if (((sides ^ old_sides) & IPUZ_STYLE_SIDES_BOTTOM) &&
      (coord.row + 1 < height))
    {
      IPuzCellCoord bottom_coord = coord;
      IPuzStyleSides bottom_sides;

      bottom_coord.row++;
      bottom_sides = ipuz_barred_get_cell_bars (self, bottom_coord);
      bottom_sides = bottom_sides ^ IPUZ_STYLE_SIDES_TOP;
      ipuz_barred_set_cell_bars (self, bottom_coord, bottom_sides);
    }

  return (old_sides != ipuz_barred_get_cell_bars (self, coord));
}

static void
ipuz_barred_fix_styles (IPuzCrossword *xword)
{
  IPuzStyle *t_style, *l_style, *tl_style;
  IPuzCellCoord coord;
  IPuzBarred *self;

  g_return_if_fail (IPUZ_IS_BARRED (xword));
  self = IPUZ_BARRED (xword);

  /* Chain up to remove any empty styles */
  IPUZ_CROSSWORD_CLASS (ipuz_barred_parent_class)->fix_styles (xword);

  /* Make sure the "t", "l", and "tl" styles exist */
  ensure_styles (self);

  coord.column = ipuz_crossword_get_width (IPUZ_CROSSWORD (self));
  coord.row = ipuz_crossword_get_height (IPUZ_CROSSWORD (self));

  /* Too small */
  if (coord.column == 0 || coord.row == 0)
    return;

  t_style = ipuz_puzzle_get_style (IPUZ_PUZZLE (self), IPUZ_BARRED_STYLE_T);
  l_style = ipuz_puzzle_get_style (IPUZ_PUZZLE (self), IPUZ_BARRED_STYLE_L);
  tl_style = ipuz_puzzle_get_style (IPUZ_PUZZLE (self), IPUZ_BARRED_STYLE_TL);
  g_return_if_fail (t_style != NULL && l_style != NULL && tl_style != NULL);

  /* This loop is rough. It's a roundabout way of going from the lower
   * right to the upper-left with guints.
   */
  do
    {
      coord.row --;
      coord.column = ipuz_crossword_get_width (IPUZ_CROSSWORD (self));
      do
        {
          IPuzCell *cell;
          IPuzStyleSides sides;
          IPuzStyle *style;
          coord.column--;

          cell = ipuz_crossword_get_cell (IPUZ_CROSSWORD (self), coord);
          style = ipuz_cell_get_style (cell);

          if (style == NULL)
            continue;
          if (style == t_style || style == l_style || style == tl_style)
            continue;

          sides = ipuz_barred_get_cell_bars (self, coord);
          if (_ipuz_style_is_empty_except_bars (style))
            {
              /* We have a style that's not one of the three internal
               * styles but just has bars. we swap it out with our
               * internal style */
              if ((sides & IPUZ_STYLE_SIDES_TOP_LEFT) == IPUZ_STYLE_SIDES_TOP_LEFT)
                ipuz_cell_set_style (cell, tl_style, IPUZ_BARRED_STYLE_TL);
              else if (sides & IPUZ_STYLE_SIDES_TOP)
                ipuz_cell_set_style (cell, t_style, IPUZ_BARRED_STYLE_T);
              else if (sides & IPUZ_STYLE_SIDES_LEFT)
                ipuz_cell_set_style (cell, l_style, IPUZ_BARRED_STYLE_L);
              else
                ipuz_cell_set_style (cell, NULL, NULL);
            }
          else
            {
              /* We leave the style mostly alone. The one change we
               * want to make is to clear the bottom and right style
               * borders if they've been set for some reason.*/
              ipuz_style_set_barred (style, sides & IPUZ_STYLE_SIDES_TOP_LEFT);
            }
        }
      while (coord.column > 0);
    }
  while (coord.row > 0);
}
