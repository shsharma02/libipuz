/* libipuz.h
 *
 * Copyright 2022 Jonathan Blandford
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#pragma once

#include <glib.h>
#include <glib-object.h>

G_BEGIN_DECLS


#include <libipuz/ipuz-acrostic.h>
#include <libipuz/ipuz-arrowword.h>
#include <libipuz/ipuz-barred.h>
#include <libipuz/ipuz-board.h>
#include <libipuz/ipuz-cell.h>
#include <libipuz/ipuz-charset.h>
#include <libipuz/ipuz-clue.h>
#include <libipuz/ipuz-crossword.h>
#include <libipuz/ipuz-cryptic.h>
#include <libipuz/ipuz-enumeration.h>
#include <libipuz/ipuz-filippine.h>
#include <libipuz/ipuz-guesses.h>
#include <libipuz/ipuz-misc.h>
#include <libipuz/ipuz-puzzle.h>
#include <libipuz/ipuz-puzzle-info.h>
#include <libipuz/libipuz-enums.h>

#define LIBIPUZ_INSIDE
# include "libipuz-version.h"
#undef LIBIPUZ_INSIDE

enum
{
  IPUZ_ERROR_INVALID_FILE,
  IPUZ_ERROR_WRONG_VERSION,
  IPUZ_ERROR_UNSUPPORTED_KIND,
};

#define IPUZ_ERROR ipuz_error_quark ()
GQuark ipuz_error_quark (void);

G_END_DECLS
