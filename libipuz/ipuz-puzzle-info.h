/* ipuz-puzzle-info.h
 *
 * Copyright 2023 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

#pragma once

#include <glib-object.h>
#include <libipuz/ipuz-charset.h>


G_BEGIN_DECLS


typedef enum
{
  IPUZ_PUZZLE_FLAG_USES_EXTENSIONS = 1 << 0, /* @ uses custom extensions to the ipuz spec @ */
  IPUZ_PUZZLE_FLAG_HAS_SOLUTION    = 1 << 1, /* @ includes the solution @ */
  IPUZ_PUZZLE_FLAG_HAS_CHECKSUM    = 1 << 2, /* @ has a checksum @ */
  IPUZ_PUZZLE_FLAG_HAS_CLUES       = 1 << 3, /* @ has clues @ */
  IPUZ_PUZZLE_FLAG_HAS_SAVED       = 1 << 4, /* @ has guesses already within the ipuz file @ */
  IPUZ_PUZZLE_FLAG_INVALID_CHARS   = 1 << 5, /* @ solution contains characters not present in the charset */
} IPuzPuzzleFlags;

typedef struct _IPuzCellStats
{
  guint block_count;
  guint normal_count;
  guint null_count;
} IPuzCellStats;


#define IPUZ_TYPE_PUZZLE_INFO (ipuz_puzzle_info_get_type ())
G_DECLARE_FINAL_TYPE (IPuzPuzzleInfo, ipuz_puzzle_info, IPUZ, PUZZLE_INFO, GObject);


IPuzPuzzleFlags  ipuz_puzzle_info_get_flags          (IPuzPuzzleInfo *self);
IPuzCellStats    ipuz_puzzle_info_get_cell_stats     (IPuzPuzzleInfo *self);
IPuzCharset     *ipuz_puzzle_info_get_charset        (IPuzPuzzleInfo *self);

/* Crossword Info */
IPuzCharset     *ipuz_puzzle_info_get_solution_chars (IPuzPuzzleInfo *self);
IPuzCharset     *ipuz_puzzle_info_get_clue_lengths   (IPuzPuzzleInfo *self);
guint            ipuz_puzzle_info_get_pangram_count  (IPuzPuzzleInfo *self);
GArray          *ipuz_puzzle_info_get_unches         (IPuzPuzzleInfo *self);


G_END_DECLS
